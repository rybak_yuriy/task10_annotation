package com.rybak.view;

import com.rybak.Annotation.MyAnnotation;
import com.rybak.controller.Controller;
import com.rybak.model.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Logger log = LogManager.getLogger(MyView.class);
    private static Scanner INPUT = new Scanner(System.in);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - test field with annotation");
        menu.put("2", " 2 - test unknown object");
        menu.put("3", " 3 - test generic");
        menu.put("4", " 4 - invoke methods");
        menu.put("5", " 5 - task 5");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::task1);
        methodsMenu.put("2", this::task2);
        methodsMenu.put("3", this::task3);
        methodsMenu.put("4", this::task4);
        methodsMenu.put("5", this::task5);
    }

    private void task1() {
        log.info("Test field with annotation");
        Class clazz = Person.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field:fields) {
            if(field.isAnnotationPresent(MyAnnotation.class)){
                MyAnnotation myAnnotation = field.getAnnotation(MyAnnotation.class);
                String name = myAnnotation.name();
                int age = myAnnotation.age();
                log.info("field= "+ field.getName());
                log.info("\tname in @MyAnnotation= " +  name);
                log.info("\tage in @MyAnnotation= " + age);
            }
        }
    }

    private void task2() {
        log.info("test unknown object");
        Person person = new Person();
        controller.getUnknownObject(person);
    }

    private void task3() {
    }

    private void task4() {
    }

    private void task5() {
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void start() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println(" Q - quit");
            System.out.print("Please, select menu point: ");
            keyMenu = INPUT.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        INPUT.close();
        System.exit(0);
    }

}
