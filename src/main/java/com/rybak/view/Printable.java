package com.rybak.view;

public interface Printable {

    void print();
}
