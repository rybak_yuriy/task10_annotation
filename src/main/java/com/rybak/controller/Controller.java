package com.rybak.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class Controller {
    private static Logger log = LogManager.getLogger(Controller.class);


    public void getUnknownObject (Object object) {
        Class unknown = object.getClass();

        Field[] declaredFields = unknown.getDeclaredFields();
        Constructor[] declaredConstructors = unknown.getDeclaredConstructors();
        Method[] declaredMethods = unknown.getDeclaredMethods();
        Class[] declaredClasses = unknown.getDeclaredClasses();
        Annotation[] declaredAnnotations = unknown.getDeclaredAnnotations();
        log.info(unknown.getName());
        for (Field declaredField : declaredFields) {
            log.info(declaredField.getType() + " " + declaredField.getName());
        }
        for (Constructor declaredConstructor : declaredConstructors) {
            log.info(declaredConstructor.getModifiers() + " " + declaredConstructor.getName());
        }
        for (Method declaredMethod : declaredMethods) {
            log.info(declaredMethod.getReturnType() + " " + declaredMethod.getName());
        }
        for (Class declaredClass : declaredClasses) {
            log.info(declaredClass.getName());
        }
        for (Annotation declaredAnnotation : declaredAnnotations) {
            log.info(declaredAnnotation.annotationType().getDeclaredFields());
        }
    }


}
